/*global define,chrome */
define([
    'framework/AbstractProvider.js'
], function (
    AbstractProvider
) {
    "use strict";

    class AuthSocialProvider extends AbstractProvider {

        constructor() {
            super('https://api.kinoafisha.info/widgetchrome/authsocial', 'POST');
        }

        map(data) {
            chrome.extension.getBackgroundPage().oauth.kinoAfishaParams = JSON.parse(data);
        }
    }

    return new AuthSocialProvider();
});