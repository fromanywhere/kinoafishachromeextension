/*global define,chrome */
define([
    'framework/AbstractProvider.js',
    'framework/ModelStorage.js'
], function (
    AbstractProvider,
    ModelStorage
) {
    "use strict";

    class AuthEmailProvider extends AbstractProvider {

        constructor() {
            super('https://api.kinoafisha.info/widgetchrome/auth', 'POST');
        }

        map(response) {

            var authData = JSON.parse(response);
            if (authData.status) {
                chrome.extension.getBackgroundPage().oauth.kinoAfishaParams = authData;
                ModelStorage.get('AppStateModel').currentState = "Main";
                ModelStorage.get('AuthModel').successEmailLogin = true;
            } else {
                ModelStorage.get('AuthModel').emailLoginError = true;
            }
        }
    }

    return new AuthEmailProvider();
});