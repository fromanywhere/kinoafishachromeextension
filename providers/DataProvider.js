/*global define,chrome */
define([
    'framework/ModelStorage.js',
    'framework/Utils.js'
], function (
    ModelStorage,
    Utils
) {
    "use strict";

    class DataProvider {

        constructor() {
            this.backgroundFeedFetch = chrome.extension.getBackgroundPage().feed;
            this.feedModel = ModelStorage.get('FeedModel');
            this.postModel = ModelStorage.get('PostModel');
        }

        map() {
            var parsedData = JSON.parse(this.backgroundFeedFetch.data);
            if (parsedData) {

                if (Array.isArray(parsedData.released) && Array.isArray(parsedData.premieres)) {
                    var i;
                    for (i = 0; i < parsedData.released.length; i++) {
                        parsedData.released[i].link = Utils.escapeJavascriptProtocolLinks(parsedData.released[i].link);
                    }

                    for (i = 0; i < parsedData.premieres.length; i++) {
                        parsedData.premieres[i].link = Utils.escapeJavascriptProtocolLinks(parsedData.premieres[i].link);
                    }

                    this.feedModel.feed = parsedData;                
                }

                if (parsedData.video && (this.postModel.link !== parsedData.video.link)) {
                    this.postModel.link = Utils.escapeJavascriptProtocolLinks(parsedData.video.link);
                    this.postModel.image = parsedData.video.poster;
                    this.postModel.date = Date.now();
                }            
            }
        }

        fetch() {
            this.map();
            this.backgroundFeedFetch.fetch('click', this.map.bind(this));
        }
    }

    return new DataProvider();
});