define([
    'models/AbstractStateModel.js'
], function (
    AbstractStateModel
) {
    "use strict";

    class AuthModel extends AbstractStateModel {

        init() {
            this.name = "AuthModel";
            this._states = ['OAuth', 'Email'];
            this._currentState = 'OAuth';
            this._emailLoginError = false;
            this._email = "";
            this._successEmailLogin = false;
        }

        get emailLoginError() {
            return this._emailLoginError;
        }

        set emailLoginError(isError) {
            this.updateField('_emailLoginError', isError);
        }

        get email() {
            return this._email;
        }

        set email(email) {
            this.updateField('_email', email);
        }

        get successEmailLogin() {
            return this._successEmailLogin;
        }

        set successEmailLogin(successEmailLogin) {
            this.updateField('_successEmailLogin', successEmailLogin);
        }
    }

    return AuthModel;
});