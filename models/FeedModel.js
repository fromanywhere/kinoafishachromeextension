define([
    'framework/AbstractModel.js'
], function (
    AbstractModel
) {
    "use strict";

    class FeedModel extends AbstractModel {

        init() {
            this.name = "FeedModel";
            this._filterStates = ['Actual', 'Premiers', 'Favorites'];
            this._currentFilterState = 'Actual';
            this._feed = {
                released: [],
                premieres: []
            };
            this._list = [];
            this._scrollPosition = 0;
        }

        get currentFilterState() {
            return this._currentFilterState;
        }

        set currentFilterState(stateName) {
            if ((this._filterStates.indexOf(stateName) !== -1) && (stateName !== this._currentFilterState)) {
                this.updateField('_currentFilterState', stateName);
            }
        }

        get feed() {
            return this._feed;
        }

        set feed(newFeed) {

            if (Array.isArray(newFeed.released) && Array.isArray(newFeed.premieres)) {
                this.updateField('_feed', newFeed);
            }
        }

        get list() {
            return this._list;
        }

        set list(list) {
            if (Array.isArray(list)) {
                this.updateField('_list', list);
            }
        }

        get scrollPosition() {
            return this._scrollPosition;
        }

        set scrollPosition(scrollPosition) {
            this.updateField('_scrollPosition', scrollPosition);
        }
    }

    return FeedModel;
});