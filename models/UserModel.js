define([
    'framework/AbstractModel.js',
    'framework/Utils.js'
], function (
    AbstractModel,
    Utils
) {
    "use strict";

    class UserModel extends AbstractModel {

        init() {
            this.name = "UserModel";
            this._userName = null;
            this._isLoginned = false;
            this._avatar = null;
            this._link = null;

            this._feedbackCount = 0;
            this._feedbackFresh = false;
            this._feedbackLink = null;

            this._likeCount = 0;
            this._likeFresh = false;
            this._likeLink = null;

            this._dislikeCount = 0;
            this._dislikeFresh = false;
            this._dislikeLink = null;
        }

        get userName() {
            return this._userName;
        }
        
        set userName(userName) {
            this.updateField('_userName', userName);
        }

        get isLoginned() {
            return this._isLoginned;
        }

        set isLoginned(isLoginned) {
            this.updateField('_isLoginned', isLoginned);
        }

        get avatar() {
            return this._avatar;
        }
        
        set avatar(avatar) {
            this.updateField('_avatar', avatar);
        }

        get link() {
            return this._link;
        }

        set link(uid) {
            var link = (String(uid).indexOf('kinoafisha') === -1)
                ? 'http://www.kinoafisha.info/user/' + uid + '/'
                : Utils.escapeJavascriptProtocolLinks(uid);

            this.updateField('_link', link);
        }
    }

    return UserModel;
});