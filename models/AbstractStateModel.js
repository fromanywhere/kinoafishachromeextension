define([
    'framework/AbstractModel.js'
], function (
    AbstractModel
) {
    "use strict";

    class AbstractStateModel extends AbstractModel {

        get currentState() {
            return this._currentState;
        }

        set currentState(stateName) {
            if (this._states.indexOf(stateName) === -1) {
                this.updateField('_currentState', this._states[0]);
            }

            if ((this._states.indexOf(stateName) !== -1) && (stateName !== this._currentState)) {
                this.updateField('_currentState', stateName);
            }
        }
    }

    return AbstractStateModel;
});