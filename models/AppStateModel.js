define([
    'models/AbstractStateModel.js'
], function (
    AbstractStateModel
) {
    "use strict";

    class AppStateModel extends AbstractStateModel {

        init() {
            this.name = "AppStateModel";
            this._states = ['Main', 'Auth'];
            this._currentState = 'Main';
        }
    }

    return AppStateModel;
});