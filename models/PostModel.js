/*global define*/
define([
    'framework/AbstractModel.js'
], function (
    AbstractModel
) {
    "use strict";

    class PostModel extends AbstractModel {

        init() {
            this.name = "PostModel";
            this._image = null;
            this._link = null;
            this._date = 0;
        }

        get image() {
            return this._image;
        }

        set image(image) {
            this.updateField('_image', image);
        }

        get link() {
            return this._link;
        }

        set link(link) {
            this.updateField('_link', link);
        }

        get date() {
            return this._date;
        }

        set date(date) {
            this.updateField('_date', date);
        }
    }

    return PostModel;
});