/*global chrome */
var oauth = (function () {
    'use strict';

    class OAuth {
        // Авторизационные данные от КА — местный uid, картинки, имя. Могут быть публичными
        get kinoAfishaParams() {
            return this._kinoAfishaParams;
        }

        set kinoAfishaParams(params) {
            this._kinoAfishaParams = params;
        }

        // Авторизационные данные из коллбэка OAuth — провайдер, uid, картинки, имя. Могут быть публичными
        get params() {
            return this._params;
        }

        set params(params) {
            this._params = params;
        }

        // Авторизационные данные из коллбэка OAuth — токен, сессия. Не могут быть публичными
        get authParams() {
            return this._authParams;
        }

        set authParams(params) {
            this._authParams = params;
        }

        openAuthPopup(role, authURL, currentConfig) {
            var tabId = null;

            // откроем вкладку и перейдем на форму авторизации. Запомним таб
            chrome.tabs.create({ url: authURL }, function (tab) {
                tabId = tab.id;
            });

            // будем следить за изменением урла нашего таба
            chrome.tabs.onUpdated.addListener((updatedTabId, changeInfo) => {

                if (tabId && tabId === updatedTabId) {
                    var url = changeInfo.url;

                    // если пользователь прошел авторизацию и его редиректнуло,
                    // проанализируем параметры
                    if (url && url.indexOf(currentConfig.redirectUri) === 0) {
                        this.authParams = this.getHashParams(url.replace(currentConfig.redirectUri, ''));

                        chrome.tabs.remove(tabId, () => {
                            if (!this.authParams.error) {
                                var url = currentConfig.makeRequest(this.authParams);
                                this.getId(role, url);
                            }
                        });
                    }

                }
            });
        }

        getHashParams(url) {

            var hashParams = {};
            var e,
                a = /\+/g,  // Regex for replacing addition symbol with a space
                r = /([^&;=]+)=?([^&;]*)/g,
                d = function (s) { return decodeURIComponent(s.replace(a, " ")); },
                q = url.substring(1);

            while (e = r.exec(q)) {
                hashParams[d(e[1])] = d(e[2]);
            }

            return hashParams;
        }

        getId(role, url) {
            var xhr = new XMLHttpRequest();
            if (role === 'ok') {
                xhr.addEventListener("load", () => {
                    var data =  JSON.parse(xhr.responseText);
                    this.params = {
                        provider: role,
                        uid: data.uid,
                        name: data.name,
                        avatar: data.pic_1
                    };
                });
                xhr.open("GET", url);
                xhr.send();
            }
            if (role === 'vk') {
                xhr.addEventListener("load", () => {
                    var data =  JSON.parse(xhr.responseText);
                    this.params = {
                        provider: role,
                        uid: data.response[0].uid,
                        name: data.response[0].first_name + ' ' + data.response[0].last_name,
                        avatar: data.response[0].photo_50
                    };
                });
                xhr.open("GET", url);
                xhr.send();
            }
            if (role === 'fb') {
                xhr.addEventListener("load", () => {
                    var data =  JSON.parse(xhr.responseText);
                    this.params = {
                        provider: role,
                        uid: data.id,
                        name: data.name,
                        avatar: data.picture.data.url
                    };
                });
                xhr.open("GET", url);
                xhr.send();
            }
        }
    }

    return new OAuth();
})();

var feed = (function () {
   'use strict';

    class BackgroundFeedFetch {

        constructor() {
            this.version = 1;
            this.timeout = null;
            this.applicationHash = null;

            var cachedVersion = localStorage.getItem('version');
            if (this.version !== cachedVersion) {
                localStorage.setItem('version', this.version);
                this.getApplicationHash().then((hash) => {
                    this.applicationHash = hash;
                    localStorage.setItem('applicationHash', this.applicationHash);
                    this.fetch('init');
                });
            } else {
                this.applicationHash = localStorage.getItem('applicationHash');
                this.fetch('background');
            }
        }

        get data() {
            return this._data;
        }

        set data(data) {
            this._data = data;
        }

        fetch(action, callback) {
            var cb = callback || function () {};
            var xhr = new XMLHttpRequest();
            var ka = oauth.kinoAfishaParams;
            var uid = (ka && ka.uid)
                ? ka.uid
                : null;

            xhr.addEventListener("load", () => {
                this.checkUpdatedCounter(xhr.responseText, action);
                this.data = xhr.responseText;
                cb();
            });
            xhr.open("GET", 'https://api.kinoafisha.info/widgetchrome/data?userid=' + uid + '&action=' + action + '&hash=' + this.applicationHash);
            xhr.send();

            clearTimeout(this.timeout);
            this.timeout = setTimeout(this.fetch.bind(this, 'background'), 1000 * 60 * 60);
        }

        checkUpdatedCounter(updatedFeed, action) {
            if (!updatedFeed || action === 'click') {
                return;
            }

            var oldFeed = localStorage.getItem('FeedModel');
            var newFeed = JSON.parse(updatedFeed);
            var oldMovieId = null;
            var counter = 0;
            if (oldFeed) {
                oldFeed = JSON.parse(oldFeed);
                if (oldFeed._feed) {

                    if (oldFeed._feed.video) {
                        oldMovieId = oldFeed._feed.video.link;
                    }

                    if (newFeed.video && newFeed.video.link && (newFeed.video.link !== oldMovieId)) {
                        counter += 1;
                    }

                    if (newFeed.new && (newFeed.new !== oldFeed._feed.new)) {
                        counter += 1;
                    }

                    if (counter) {
                        chrome.browserAction.setBadgeText({
                            text: 'new'
                        });
                    } else {
                        chrome.browserAction.setBadgeText({
                            'text': ''
                        });
                    }
                }
            }
        }

        sha256hash(text) {
            function sha256(str) {
                // We transform the string into an arraybuffer.
                var buffer = new TextEncoder("utf-8").encode(str);
                return window.crypto.subtle.digest("SHA-256", buffer).then(function (hash) {
                    return hex(hash);
                });
            }

            function hex(buffer) {
                var hexCodes = [];
                var view = new DataView(buffer);
                for (var i = 0; i < view.byteLength; i += 4) {
                    // Using getUint32 reduces the number of iterations needed (we process 4 bytes each time)
                    var value = view.getUint32(i);
                    // toString(16) will give the hex representation of the number without padding
                    var stringValue = value.toString(16);
                    // We use concatenation and slice for padding
                    var padding = '00000000';
                    var paddedValue = (padding + stringValue).slice(-padding.length);
                    hexCodes.push(paddedValue);
                }

                // Join all the hex strings into one
                return hexCodes.join("");
            }

            return sha256(text);
        }

        getApplicationHash() {
            var randomString = Math.random();
            var now = Date.now();

            return this.sha256hash([randomString, now].join());
        }
    }

   return new BackgroundFeedFetch();
})();