/*global browser,require */
require([
    'framework/Utils.js',
    'framework/ComponentStorage.js',
    'framework/ModelStorage.js',
    'models/AppStateModel.js',
    'models/AuthModel.js',
    'models/UserModel.js',
    'models/FeedModel.js',
    'models/PostModel.js'
], function (
    Utils,
    ComponentStorage,
    ModelStorage,
    AppStateModel,
    AuthModel,
    UserModel,
    FeedModel,
    PostModel
) {
    'use strict';

    window.app = {
        components: ComponentStorage
    };

    ModelStorage.add(new AppStateModel());
    ModelStorage.add(new AuthModel());
    ModelStorage.add(new UserModel());
    ModelStorage.add(new FeedModel());
    ModelStorage.add(new PostModel());

    Utils.activateComponents(document.body);
});