/*global define */
define(function () {
    "use strict";

    class ComponentStorage {
        constructor() {
            this.components = {};
            this._increment = 1;
        }

        get uniqId() {
            return this._increment++;
        }

        add(component) {
            if (component.id) {
                this.components[component.id] = component;
            } else {
                throw new Error('Component ' + component + ' has incorrect id');
            }
        }

        get(componentId) {
            if (this.components[componentId]) {
                return this.components[componentId];
            }
            throw new Error('Component ' + componentId + ' was not found');
        }

        remove(componentId) {
            if (this.components[componentId]) {
                delete this.components[componentId];
            } else {
                throw new Error('Component ' + componentId + ' was not found');
            }
        }
    }

    return new ComponentStorage();
});