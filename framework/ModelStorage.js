/*global define */
define(function () {
    "use strict";

    class ModelStorage {
        constructor() {
            this.models = {};
        }

        add(model) {
            this.models[model.name] = model;
        }

        get(modelName) {
            if (!this.models[modelName]) {
                throw new Error('Seems like model is undefined');
            }
            return this.models[modelName];
        }
    }

    return new ModelStorage();
});

