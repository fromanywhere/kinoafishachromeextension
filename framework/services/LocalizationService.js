/*global define */
define(function () {
    "use strict";

    class LocalizationService {

        get dictionary() {
            return {
                'button_logout': 'Выйти',
                'button_enter': 'Вход',
                'button_enter2': 'Войти',
                'button_back': 'Назад',
                'button_cancel': 'Отмена',

                'widget_feedback': 'Отзывы',

                'tabs_actual': 'В прокате',
                'tabs_premiers': 'Премьеры',
                'tabs_favorites': 'Избранное',
                'tabs_enter': 'Вход через',
                'tabs_socials': 'Соц. сети',
                'tabs_email': 'E-mail',

                'date_today': 'Сегодня',
                'date_tomorrow': 'Завтра',

                'title_auth': 'Авторизация',

                'placeholders_email': 'Ваш E-mail',
                'placeholder_password': '&bull;&bull;&bull;&bull;&bull;&bull;&bull;&bull;',

                'validation_incorrect': 'Неверный пароль или имя пользователя',

                'link_register': 'Регистрация',
                'link_recover': 'Восстановление пароля',

                'text_favorites': 'Войдите, чтобы видеть избранные фильмы',
                'text_post': 'Видео&shy;обзор премьер',
                'text_new': 'Новый',

                'stub_feed_favorites': 'Добавьте фильмы в избранное на ',
                'stub_feed_all': 'Раздел пуст. Возможно, нет соединения с Интернетом',

                'utm': 'utm_source=ch_tb&Utm_medium=ch_tb&Utm_campaign=ch_tb'
            };
        }
        
        formatMonth(date) {
            var day = date.getDate();
            var month = date.getMonth();
            switch(month) {
                case 0:
                    return day + ' января';
                case 1:
                    return day + ' февраля';
                case 2:
                    return day + ' марта';
                case 3:
                    return day + ' апреля';
                case 4:
                    return day + ' мая';
                case 5:
                    return day + ' июня';
                case 6:
                    return day + ' июля';
                case 7:
                    return day + ' августа';
                case 8:
                    return day + ' сентября';
                case 9:
                    return day + ' октября';
                case 10:
                    return day + ' ноября';
                case 11:
                    return day + ' декабря';
            }        
        }
        
        getDateString(date) {
            var str = '';
            var now = new Date();
            var today = new Date(now.getFullYear(), now.getMonth(), now.getDate());
            var tomorrow = new Date(now.getFullYear(), now.getMonth(), now.getDate());
            var tomorrow2 = new Date(now.getFullYear(), now.getMonth(), now.getDate());

            tomorrow.setDate(today.getDate() + 1);
            tomorrow2.setDate(today.getDate() + 2);

            if ((date < tomorrow2) || (date === tomorrow)) {
                str = 'Завтра';
            }

            if (date < tomorrow) {
                str = 'Сегодня';
            }

            if (date >= tomorrow2) {
                str = 'c ' + this.formatMonth(date);
            }
            return str;
        }

        hyphenate(text) {
            var RusA = "[абвгдеёжзийклмнопрстуфхцчшщъыьэюя]";
            var RusV = "[аеёиоуыэю\я]";
            var RusN = "[бвгджзклмнпрстфхцчшщ]";
            var RusX = "[йъь]";

            var re1 = new RegExp("("+RusX+")("+RusA+RusA+")","ig");
            var re2 = new RegExp("("+RusV+")("+RusV+RusA+")","ig");
            var re3 = new RegExp("("+RusV+RusN+")("+RusN+RusV+")","ig");
            var re4 = new RegExp("("+RusN+RusV+")("+RusN+RusV+")","ig");
            var re5 = new RegExp("("+RusV+RusN+")("+RusN+RusN+RusV+")","ig");
            var re6 = new RegExp("("+RusV+RusN+RusN+")("+RusN+RusN+RusV+")","ig");

            text = text.replace(re1, "$1\xAD$2");
            text = text.replace(re2, "$1\xAD$2");
            text = text.replace(re3, "$1\xAD$2");
            text = text.replace(re4, "$1\xAD$2");
            text = text.replace(re5, "$1\xAD$2");
            text = text.replace(re6, "$1\xAD$2");

            return text;
        }
    }

    return new LocalizationService();
});