/*global define */
define([
    'framework/ModelStorage.js',
], function (
    ModelStorage
) {
    "use strict";

    class Observer {

        // создать хранилище отслеживаемых полей и моделей
        constructor() {
            this.watching = {};
        }

        // оповестить об изменении
        change(modelName, modelFieldName) {
            var watching = this.watching;
            var actionKey = modelName + '_' + modelFieldName;

            if (watching[actionKey]) {
                watching[actionKey].forEach((action) => action(ModelStorage.get(modelName)[modelFieldName]));
            }
        }

        // подписаться на изменения
        subscribe(modelName, modelFieldName, action) {
            var actionKey = modelName + '_' + modelFieldName;

            this.watching[actionKey] = this.watching[actionKey] || [];
            this.watching[actionKey].push(action);
        }

        // отписаться от изменений
        unsubscribe(modelName, modelFieldName, action) {
            var actionKey = modelName + '_' + modelFieldName;
            var index = this.watching[actionKey].indexOf(action);
            if (index !== -1) {
                this.watching[actionKey].splice(index, 1);
            }
        }

    }

    return new Observer();
});