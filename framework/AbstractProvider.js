/*global define */
define(function () {
    "use strict";

    class AbstractProvider {

        static convertParamObjectToString(obj) {
            var str = [];
            for(var p in obj) {
                if (obj.hasOwnProperty(p)) {
                    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                }
            }
            return str.join("&");
        }

        constructor(url, method) {
            this.url = url;
            this.method = method;
            this.fetchFinishAction = () => {};
        }

        onFetchStart() {}

        onFetchFinish(response) {
            this.fetchFinishAction(response);
        }

        fetch(data) {
            this.onFetchStart();

            var sendData = this.method === 'GET'
                ? null
                : JSON.stringify(data);

            var sendParams = this.method === 'GET'
                ? '?' + AbstractProvider.convertParamObjectToString(data)
                : "";

            var xhr = new XMLHttpRequest();
            xhr.open(this.method, this.url + sendParams, true);
            xhr.onload = () => {
                this.map(xhr.responseText);
                this.onFetchFinish(xhr.responseText);
            };
            xhr.send(sendData);

            return this;
        }

        setFetchFinish(action) {
            this.fetchFinishAction = action;
            return this;
        }

        map() {}

    }

    return AbstractProvider;
});