/*global define */
define([
    'framework/Utils.js',
    'framework/ComponentStorage.js',
    'framework/ModelStorage.js',
    'framework/Observer.js',
    'framework/services/LocalizationService.js',
    'doT'
], function (
    Utils,
    ComponentStorage,
    ModelStorage,
    Observer,
    LocalizationService,
    doT
) {
    "use strict";

    class AbstractComponent {

        constructor(scope) {
            // Для уникальной идентификации экземпляра. При обновлении экземпляра сохраняется
            this.id = ComponentStorage.uniqId;
            ComponentStorage.add(this);

            // Модель данных шаблона. Может быть задана в разметке
            this.scope = Object.assign({
                    instance: "app.components.get('" + this.id + "')" ,
                    locale: LocalizationService.dictionary
                },
                scope
            );
            this.bindings = [];

            this.setTemplate();
            this.init();
        }

        destructor() {
            this.unbindFields();
            this.deactivate();
            this.deactivateChildComponents();
            ComponentStorage.remove(this.id);
        }

        get template() {
            throw new Error("Component " + this.constructor.name + " hasn't template");
        }

        // Получить отрендеренный компонент
        get html() {
            return (this.compiledTemplate)
                ? this.compiledTemplate(this.scope)
                : "";
        }

        setTemplate() {
            if (this.template) {
                var cleanTemplate = this.template.replace(/<!--[\s\S]*?-->/g, "");
                this.compiledTemplate = doT.template(cleanTemplate);
            }
        }

        // Проинициализировать модель до вставки
        init() {}

        // Активировать вложенные компоненты
        activateChildComponents() {
            Utils.activateComponents(this.node);
        }

        deactivateChildComponents() {
            Utils.deactivateComponents(this.node);
        }

        activate() {}

        deactivate() {}

        // Вставка компонента в дерево
        mount(node) {
            this.replaceNode(node);
            this.activateChildComponents();
            this.activate();
            return this.node;
        }

        // Служебный метод, обновляющий компонент при изменении и инициализации
        replaceNode(oldNode) {
            var html = this.html;
            if (html) {
                var parentNode = oldNode.parentNode;
                var newNode = document.createRange().createContextualFragment(html);

                if (!newNode.children.length) {
                    newNode = document.createRange().createContextualFragment("<div>" + html + "</div>");
                }

                newNode.lastChild.setAttribute('data-component-id', this.id);
                newNode.lastChild.setAttribute('data-component-name', this.constructor.name);

                parentNode.replaceChild(newNode, oldNode);
                this.node = document.querySelector('[data-component-id="' + this.id + '"]');
                this.cachedHtml = html;
            }
        }

        // Обновить компонент
        render() {
            if (this.cachedHtml !== this.html && this.node) {
                this.deactivate();
                this.deactivateChildComponents();
                this.replaceNode(this.node);
                this.activateChildComponents();
                this.activate();
            }
        }

        //Связать поле модели шаблона с любой другой «уникальной» моделью
        bindField(componentField, modelName, modelFieldName, action) {
            var bindings = this.bindings;

            var fieldChangeAction = function bind(value) {
                this.scope[componentField] = value;
            }.bind(this);

            function processAction(action) {
                bindings.push({
                    modelName: modelName,
                    modelFieldName: modelFieldName,
                    action: action
                });

                Observer.subscribe(modelName, modelFieldName, action);
            }

            processAction(fieldChangeAction);

            this.scope[componentField] = ModelStorage.get(modelName)[modelFieldName];

            if (action) {
                processAction(action);
                action();
            }
        }

        unbindFields() {
            for (var actionCounter = 0; actionCounter < this.bindings.length; actionCounter++) {
                var subscribe = this.bindings[actionCounter];
                Observer.unsubscribe(subscribe.modelName, subscribe.modelFieldName, subscribe.action);
            }
        }
    }

    return AbstractComponent;

});