/*global define,require */
define([
    'framework/ComponentStorage.js',
], function (
    ComponentStorage
) {
    "use strict";

    class Utils {

        // http://stackoverflow.com/questions/9036429/convert-object-string-to-json
        JSONize(str) {
            return str
            // wrap keys without quote with valid double quote
                .replace(/([\$\w]+)\s*:/g, function (_, $1){return '"'+$1+'":';})
                // replacing single quote wrapped ones to double quote
                .replace(/''/g, '""')
                .replace(/'([^']+)'/g, function (_, $1){return '"'+$1+'"';});
        }

        // 'javascript:' -> # in links
        escapeJavascriptProtocolLinks(link) {
            return link.indexOf("http://") === 0 || link.indexOf("https://") === 0
                ? link
                : "#";
        }

        activateComponents(rootNode) {
            var componentsList = rootNode.querySelectorAll('[data-component]');
            for (var i = 0; i < componentsList.length; i++) {
                var currentComponentNode = componentsList[i];
                var currentComponentName = currentComponentNode.getAttribute('data-component');
                var currentComponentModel = currentComponentNode.getAttribute('data-model');

                currentComponentModel = (currentComponentModel)
                    ? JSON.parse(this.JSONize(currentComponentModel))
                    : {};

                var Component = require('components/' + currentComponentName + '/' + currentComponentName + '.js');
                new Component(currentComponentModel).mount(currentComponentNode);
            }
        }

        deactivateComponents(rootNode) {
            var componentsList = rootNode.querySelectorAll('[data-component-id]');
            var excludedList = [];
            for (var i = 0; i < componentsList.length; i++) {
                var currentComponentNode = componentsList[i];
                var excludedSubList = Array.from(currentComponentNode.querySelectorAll('[data-component-id]'));

                excludedSubList.forEach((excludedElement) => {
                    if (excludedList.indexOf(excludedElement) === -1) {
                        excludedList.push(excludedElement);
                    }
                });

                if (excludedList.indexOf(currentComponentNode) === -1) {
                    var currentComponentName = currentComponentNode.getAttribute('data-component-id');
                    ComponentStorage.get(currentComponentName).destructor();
                }
            }
        }
    }

    return new Utils();
});