/*global define */
define([
    'framework/Observer.js'
], function (
    Observer
) {
    "use strict";

    class AbstractModel {
        constructor() {
            // this.bindings содержит ссылки на экз. компонентов, использующих инстанс модели
            this._modelName = null;
            this.cache = {};
            this.init();
            this.getCache();
        }
        
        get name() {
            if (this._modelName) {
                return this._modelName;
            } else {
                throw new Error("Model name must be overrided by instance due Firefox ES6 limitations");
            }
        }

        set name(name) {
            if (name) {
                this._modelName = name;
            }
        }

        // Вызывать для удаления модели
        destructor() {

        }

        init() {}

        setCache() {
            var stringModel = JSON.stringify(this.cache);
            localStorage.setItem(this.name, stringModel);
        }

        getCache() {
            var objectModel = JSON.parse(localStorage.getItem(this.name));
            for (var key in objectModel) {
                if (objectModel.hasOwnProperty(key)) {
                    var fieldName = key.substring(1);
                    this[fieldName] = objectModel[key];
                    this[fieldName + '_cache'] = objectModel[key];
                }
            }
        }

        // оповестить об изменении поля модели
        updateField(fieldName, value) {
            var strValue = JSON.stringify(value);
            if (this[fieldName + '_cache'] !== strValue) {
                this[fieldName] = value;
                this[fieldName + '_cache'] = strValue;
                this.cache[fieldName] = value;
                this.setCache();
                Observer.change(this.name, fieldName.substring(1));
            }
        }
    }

    return AbstractModel;
});