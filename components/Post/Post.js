/*global define */
define([
    'framework/AbstractComponent.js',
    'framework/ModelStorage.js',
    'text!./Post.html'
], function (
    AbstractComponent,
    ModelStorage,
    template
) {
    "use strict";

    class Post extends AbstractComponent {

        get template() {
            return template;
        }

        init() {
            this.scope.new = false;
            this.bindField('image', 'PostModel', 'image');
            this.bindField('link', 'PostModel', 'link');
            this.bindField(
                'date',
                'PostModel',
                'date',
                function render() {
                    this.scope.new = this.scope.date > (Date.now() - 1000 * 60 * 60 * 72);
                    this.render();
                }.bind(this)
            );
        }

        activate() {
            // Если это расширение, открыть в новой вкладке и сбросить плашку
            this.onClick = function onClick(e) {
                if (chrome && chrome.tabs) {
                    e.preventDefault();

                    var href = this.node.getAttribute('href');
                    if (href) {
                        chrome.tabs.create({ url: href }, function () {
                            ModelStorage.get('PostModel').date = 0;
                        });
                    }
                }
            }.bind(this);

            this.node.addEventListener('click', this.onClick, false);
        }

        deactivate() {
            this.node.removeEventListener('click', this.onClick);
        }
    }

    return Post;
});