/*global define */
define([
    'framework/AbstractComponent.js',
    'text!./Header.html'
], function (
    AbstractComponent,
    template
) {
    "use strict";

    class Header extends AbstractComponent {

        get template() {
            return template;
        }
    }

    return Header;
});