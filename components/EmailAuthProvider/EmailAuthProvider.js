/*global define */
define([
    'framework/AbstractComponent.js',
    'framework/ModelStorage.js',
    'providers/AuthEmailProvider.js',
    'text!./EmailAuthProvider.html'
], function (
    AbstractComponent,
    ModelStorage,
    AuthEmailProvider,
    template
) {
    "use strict";

    class EmailAuthProvider extends AbstractComponent {

        get template() {
            return template;
        }

        init() {
            this.authModel = ModelStorage.get('AuthModel');
            this.authSocialProvider = AuthEmailProvider;

            this.bindField(
                'error',
                'AuthModel',
                'emailLoginError',
                this.render.bind(this)
            );

            this.bindField(
                'email',
                'AuthModel',
                'email'
            );

            this.onSubmit = function onSubmit(e) {
                e.preventDefault();

                if (this.loginField && this.passwordField) {
                    var loginData =  {
                        login: this.loginField.value,
                        password: this.passwordField.value
                    };

                    this.authModel.email = this.loginField.value;
                    this.authSocialProvider.fetch(loginData);
                } else {
                    this.authModel.emailLoginError = true;
                }
            }.bind(this);
        }

        activate() {
            this.form = this.node.querySelector('form');
            this.form.addEventListener('submit', this.onSubmit, false);

            this.loginField = this.form.querySelector('[name="email"]');
            this.passwordField = this.form.querySelector('[name="password"]');
        }

        deactivate() {
            this.form.removeEventListener('submit', this.onSubmit);
        }
    }

    return EmailAuthProvider;
});