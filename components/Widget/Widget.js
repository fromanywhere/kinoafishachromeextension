/*global define */
define([
    'framework/AbstractComponent.js',
    'text!./Widget.html'
], function (
    AbstractComponent,
    template
) {
    "use strict";

    class Widget extends AbstractComponent {

        get template() {
            return template;
        }
    }

    return Widget;
});