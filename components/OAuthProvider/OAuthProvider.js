/*global define */
define([
    'framework/AbstractComponent.js',
    'doT',
    './md5.js',
    'text!./OAuthProvider.html',
    'text!./OAuthODKL.html',
    'text!./OAuthVK.html',
    'text!./OAuthFB.html'
], function (
    AbstractComponent,
    doT,
    md5,
    template,
    OAuthODKLUrl,
    OAuthVKUrl,
    OAuthFBUrl
) {
    "use strict";

    class OAuthProvider extends AbstractComponent {

        get template() {
            return template;
        }

        static get providersConfig() {
            return {
                ok: {
                    clientId: '1153635840',
                    scope: 'VALUABLE_ACCESS',
                    responseType: 'token',
                    publicKey: 'CBAHBEMFEBABABABA',
                    method: 'users.getCurrentUser',
                    authURLTemplate: OAuthODKLUrl,
                    redirectUri: 'http://api.ok.ru/blank.html',
                    makeRequest(params){
                        var concatStr = 'application_key=' + this.publicKey + 'format=JSONmethod=' + this.method + params.session_secret_key;
                        var sig = md5(concatStr);
                        return 'http://api.ok.ru/api/users/getCurrentUser?application_key=' + this.publicKey + '&method=' + this.method + '&access_token=' + params.access_token + '&format=JSON&sig=' + sig;
                    }
                },
                vk: {
                    clientId: '5061417',
                    responseType: 'token',
                    authURLTemplate: OAuthVKUrl,
                    redirectUri: 'https://oauth.vk.com/blank.html',
                    makeRequest(params) {
                        return 'https://api.vk.com/method/users.get?user_ids=' + params.user_id + '&fields=photo_50&access_token=' + params.access_token;
                    }
                },
                fb: {
                    clientId: '533317036835537',
                    responseType: 'token',
                    authURLTemplate: OAuthFBUrl,
                    redirectUri: 'https://www.facebook.com/connect/login_success.html',
                    makeRequest(params) {
                        return 'https://graph.facebook.com/me?fields=id,name,picture&access_token=' + params.access_token;
                    }
                }
            };
        }

        activate() {
            this.clickAction = function click() {
                this.currentConfig = OAuthProvider.providersConfig[this.scope.role];
                this.authURL = doT.template(this.currentConfig.authURLTemplate)(this.currentConfig);

                var bgPage = chrome.extension.getBackgroundPage();
                bgPage.oauth.openAuthPopup(this.scope.role, this.authURL, this.currentConfig);
            }.bind(this);

            this.node.addEventListener('click', this.clickAction, false);
        }

        deactivate() {
            this.node.removeEventListener('click', this.clickAction);
        }
    }

    return OAuthProvider;
});