/*global define */
define([
    'framework/AbstractComponent.js',
    'text!./User.html'
], function (
    AbstractComponent,
    template
) {
    "use strict";

    class User extends AbstractComponent {

        get template() {
            return template;
        }

        init() {
            this.appState = null;

            this.bindField(
                'userName',
                'UserModel',
                'userName',
                this.render.bind(this)
            );

            this.bindField(
                'isLoginned',
                'UserModel',
                'isLoginned',
                this.render.bind(this)
            );

            this.bindField(
                'avatar',
                'UserModel',
                'avatar',
                this.render.bind(this)
            );

            this.bindField(
                'link',
                'UserModel',
                'link',
                this.render.bind(this)
            );
        }
    }

    return User;
});