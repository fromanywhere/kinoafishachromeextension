/*global define */
define([
    'framework/AbstractComponent.js',
    'text!./Widgets.html'
], function (
    AbstractComponent,
    template
) {
    "use strict";

    class Widgets extends AbstractComponent {

        get template() {
            return template;
        }
    }

    return Widgets;
});