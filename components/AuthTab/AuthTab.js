/*global define */
define([
    'framework/AbstractComponent.js',
    'framework/ModelStorage.js',
    'text!./AuthTab.html'
], function (
    AbstractComponent,
    ModelStorage,
    template
) {
    "use strict";

    class AuthTabs extends AbstractComponent {

        get template() {
            return template;
        }

        init() {
            this.filterState = null;
            this.clickAction = function click() {
                ModelStorage.get('AuthModel').currentState = this.scope.role;
            }.bind(this);

            this.bindField(
                'filterState',
                'AuthModel',
                'currentState',
                function render() {
                    this.scope.active = this.scope.filterState === this.scope.role;
                    this.render();
                }.bind(this)
            );
        }

        activate() {
            this.node.addEventListener('click', this.clickAction, false);
        }

        deactivate() {
            this.node.removeEventListener('click', this.clickAction);
        }
    }

    return AuthTabs;
});