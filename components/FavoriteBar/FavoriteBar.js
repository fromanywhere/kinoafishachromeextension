/*global define */
define([
    'framework/AbstractComponent.js',
    'framework/ModelStorage.js',
    'text!./FavoriteBar.html'
], function (
    AbstractComponent,
    ModelStorage,
    template
) {
    "use strict";

    class FavoriteBar extends AbstractComponent {

        get template() {
            return template;
        }

        activate() {
            this.loginLink = this.node.querySelector('.favorite-bar_text');

            this.onClick = function onClick() {
                this.appStateModel = ModelStorage.get('AppStateModel').currentState = 'Auth';
            }.bind(this);

            this.loginLink.addEventListener('click', this.onClick, false);
        }

        deactivate() {
            this.loginLink.removeEventListener('click', this.onClick);
        }
    }

    return FavoriteBar;
});