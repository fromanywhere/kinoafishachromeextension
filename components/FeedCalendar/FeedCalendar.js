/*global define */
define([
    'framework/AbstractComponent.js',
    'framework/services/LocalizationService.js',
    'framework/ModelStorage.js',
    'text!./FeedCalendar.html'
], function (
    AbstractComponent,
    LocalizationService,
    ModelStorage,
    template
) {
    "use strict";

    class FeedCalendar extends AbstractComponent {

        get template() {
            return template;
        }

        init() {
            this.bindField(
                'list',
                'FeedModel',
                'list',
                function render () {
                    this.scope.dateList = this.reduceDates(this.scope.list);
                    this.showCalendar();
                    this.render();
                }.bind(this)
            );
        }

        activate() {
            this.icon = this.node.querySelector('.feed-calendar_ac');
            this.list = this.node.querySelector('.feed-calendar_list');

            this.onClickIcon = function (e) {
                e.stopPropagation();
                this.list.classList.toggle('__active');
            }.bind(this);

            this.onClickWrapper = function () {
                this.list.classList.remove('__active');
            }.bind(this);

            this.onClickItem = function (e) {
                var itemDate = e.target.getAttribute('data-date');
                itemDate = (itemDate === 'null')
                    ? null
                    : itemDate;

                document.dispatchEvent(new CustomEvent('feedCalendarSelect', { 'detail': itemDate }));

            }.bind(this);

            if (this.icon) {
                this.icon.addEventListener('click', this.onClickIcon, false);
            }

            if (this.list) {
                this.list.addEventListener('click', this.onClickItem, false);
                document.addEventListener('wrapperClick', this.onClickWrapper, false);
            }
        }

        deactivate() {
            if (this.icon) {
                this.icon.removeEventListener('click', this.onClickIcon);
            }

            if (this.list) {
                this.list.removeEventListener('click', this.onClickItem);
                document.removeEventListener('wrapperClick', this.onClickWrapper);
            }
        }

        showCalendar() {
            this.scope.showCalendar = (this.scope.dateList && this.scope.dateList.length > 1);
        }

        reduceDates(feed) {
            var keys = [];
            var result = [];

            feed.forEach((item) => {
                var date = new Date(item.parsedDate);

                if (keys.indexOf(date.getDate()) === -1) {
                    result.push({
                        parsedDate: date,
                        formatDate: LocalizationService.getDateString(date)
                    });

                    keys.push(date.getDate());
                }
            });

            return result;
        }
    }

    return FeedCalendar;
});