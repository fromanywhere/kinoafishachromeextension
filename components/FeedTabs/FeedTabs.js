/*global define */
define([
    'framework/AbstractComponent.js',
    'framework/ModelStorage.js',
    'text!./FeedTabs.html'
], function (
    AbstractComponent,
    ModelStorage,
    template
) {
    "use strict";

    class FeedTabs extends AbstractComponent {

        get template() {
            return template;
        }
        
        init() {
            this.bindField(
                'currentFilterState',
                'FeedModel',
                'currentFilterState'
            );

            this.bindField(
                'isLoginned',
                'UserModel',
                'isLoginned',
                function unFavoriteTab() {
                    if (!this.scope.isLoginned && this.scope.currentFilterState === 'Favorites') {
                        ModelStorage.get('FeedModel').currentFilterState = 'Actual';
                    }
                }.bind(this)
            );
        }
    }

    return FeedTabs;
});