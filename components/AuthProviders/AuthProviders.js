/*global define */
define([
    'framework/AbstractComponent.js',
    'framework/ModelStorage.js',
    'text!./AuthProviders.html'
], function (
    AbstractComponent,
    ModelStorage,
    template
) {
    "use strict";

    class AuthProviders extends AbstractComponent {

        get template() {
            return template;
        }

        init() {
            this.authState = null;
            this.bindField(
                'authState',
                'AuthModel',
                'currentState',
                this.render.bind(this)
            );

            ModelStorage.get('AuthModel').emailLoginError = false;
            ModelStorage.get('AuthModel').email = '';
        }
    }

    return AuthProviders;
});