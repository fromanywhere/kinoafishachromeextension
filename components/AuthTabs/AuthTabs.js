/*global define */
define([
    'framework/AbstractComponent.js',
    'text!./AuthTabs.html'
], function (
    AbstractComponent,
    template
) {
    "use strict";

    class AuthTabs extends AbstractComponent {

        get template() {
            return template;
        }
    }

    return AuthTabs;
});