/*global define */
define([
    'framework/AbstractComponent.js',
    'text!./Feed.html'
], function (
    AbstractComponent,
    template
) {
    "use strict";

    class Feed extends AbstractComponent {

        get template() {
            return template;
        }
    }

    return Feed;
});