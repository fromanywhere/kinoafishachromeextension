/*global define */
define([
    'framework/AbstractComponent.js',
    'text!./Button.html'
], function (
    AbstractComponent,
    template
) {
    "use strict";

    class Button extends AbstractComponent {

        get template() {
            return template;
        }

        init() {
            this.clickAction = this.click.bind(this);
            this.scope.classname = this.scope.classname || "button";
        }

        activate() {
            this.node.addEventListener('click', this.clickAction, false);
        }

        deactivate() {
            this.node.removeEventListener('click', this.clickAction);
        }

        click() {}
    }

    return Button;
});