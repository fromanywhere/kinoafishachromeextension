/*global define */
define([
    '../Button/Button.js'
], function (
    Button
) {
    "use strict";

    class LoginButton extends Button {

        init () {
            super.init();
            this.scope.key = 'button_enter2';
            this.scope.mod = '__action';
        }
    }

    return LoginButton;
});