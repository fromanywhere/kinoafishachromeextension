/*global define */
define([
    'framework/AbstractComponent.js',
    'framework/ModelStorage.js',
    'text!./FeedTab.html'
], function (
    AbstractComponent,
    ModelStorage,
    template
) {
    "use strict";

    class FeedTab extends AbstractComponent {

        get template() {
            return template;
        }

        init() {

            this.filterState = null;
            this.clickAction = function click() {
                ModelStorage.get('FeedModel').currentFilterState = this.scope.role;
            }.bind(this);

            this.bindField(
                'filterState',
                'FeedModel',
                'currentFilterState',
                function render() {
                    this.scope.active = this.scope.filterState === this.scope.role;
                    this.render();
                }.bind(this)
            );
        }

        activate() {
            this.node.addEventListener('click', this.clickAction, false);
        }
        
        deactivate() {
            this.node.removeEventListener('click', this.clickAction);
        }
    }

    return FeedTab;
});