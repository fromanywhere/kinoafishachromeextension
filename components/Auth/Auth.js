/*global define */
define([
    'framework/AbstractComponent.js',
    'text!./Auth.html'
], function (
    AbstractComponent,
    template
) {
    "use strict";

    class Auth extends AbstractComponent {

        get template() {
            return template;
        }
    }

    return Auth;
});