/*global define */
define([
    '../Button/Button.js',
    'framework/ModelStorage.js'
], function (
    Button,
    ModelStorage
) {
    "use strict";

    class LogoutButton extends Button {

        init() {
            super.init();
            this.userModel = ModelStorage.get('UserModel');
            this.scope.classname = 'logout';
            this.scope.key = false;
        }

        click() {
            var bgPage = chrome.extension.getBackgroundPage();

            // Facebook умеет разлогинивать приложение (отзывом прав), а остальные нет
            // С другой стороны, все остальные умеют показывать диалог смены аккаунта при логине, а FB без разлогина — нет
            // Так что для возможности залогиниться другим юзером в FB делаем отзыв прав
            if (bgPage.oauth.params && (bgPage.oauth.params.provider === 'fb')) {
                var xhr = new XMLHttpRequest();
                xhr.open("DELETE", 'https://graph.facebook.com/me/permissions');
                xhr.send('access_token=' + bgPage.oauth.authParams['?#access_token']);
            }

            ModelStorage.get('AuthModel').successEmailLogin = false;

            bgPage.oauth.params = null;
            bgPage.oauth.kinoAfishaParams = null;
            bgPage.oauth.authParams = null;

            this.userModel.userName = null;
            this.userModel.avatar = null;
            this.userModel.link = null;
            this.userModel.isLoginned = false;
        }
    }

    return LogoutButton;
});