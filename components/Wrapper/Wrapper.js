/*global define, chrome */
define([
    'framework/AbstractComponent.js',
    'framework/ModelStorage.js',
    'providers/DataProvider.js',
    'providers/AuthSocialProvider.js',
    'text!./Wrapper.html'
], function (
    AbstractComponent,
    ModelStorage,
    DataProvider,
    AuthSocialProvider,
    template
) {
    "use strict";

    class Wrapper extends AbstractComponent {

        get template() {
            return template;
        }

        init() {
            this.appState = null;
            this.dataProvider = DataProvider;
            this.authSocialProvider = AuthSocialProvider;
            this.bgPage = chrome.extension.getBackgroundPage();
            this.appStateModel = ModelStorage.get('AppStateModel');
            this.userModel = ModelStorage.get('UserModel');

            this.onClick = () => {
                document.dispatchEvent(new Event('wrapperClick'));
            };

            this.bindField(
                'appState',
                'AppStateModel',
                'currentState',
                this.render.bind(this)
            );

            this.bindField(
                'isLoginned',
                'UserModel',
                'isLoginned',
                this.render.bind(this)
            );

            this.bindField(
                'successEmailLogin',
                'AuthModel',
                'successEmailLogin',
                this.loginInit.bind(this) // Вызовется всегда
            );

            chrome.browserAction.setBadgeText({
                'text': ''
            });
        }

        loginInit() {

            if (!this.userModel.isLoginned) {
                var params = this.bgPage.oauth.params;
                var ka = this.bgPage.oauth.kinoAfishaParams;
                var fetchFeedAction = this.fetchFeed.bind(this);

                if (params) {
                    this.appStateModel.currentState = 'Main';
                    this.authSocialProvider
                        .setFetchFinish(fetchFeedAction)
                        .fetch(params);
                } else {
                    if (ka && ka.uid) {
                        this.appStateModel.currentState = 'Main';
                        this.userModel.isLoginned = true;
                    }
                    this.fetchFeed();
                }
            } else {
                this.appStateModel.currentState = 'Main';
                this.fetchFeed();
            }
        }

        fetchFeed() {
            var ka = this.bgPage.oauth.kinoAfishaParams;
            var uid = ka
                ? ka.uid
                : null;

            var oauthParams = this.bgPage.oauth.params;
            if (oauthParams || (ka && ka.status)) {
                this.userModel.userName = oauthParams && oauthParams.name;
                this.userModel.avatar = oauthParams && oauthParams.avatar;
                if (uid) {
                    this.userModel.link = uid;
                    this.userModel.userName = this.userModel.userName || ka.name;
                    this.userModel.avatar = this.userModel.avatar || 'http://kinoafisha.info' + ka.avatar.small;
                }
                this.userModel.isLoginned = true;
            }

            this.dataProvider.fetch();
        }

        activate() {
            this.node.addEventListener('click', this.onClick, false);
        }

        deactivate() {
            this.node.removeEventListener('click', this.onClick);
        }
    }

    return Wrapper;
});