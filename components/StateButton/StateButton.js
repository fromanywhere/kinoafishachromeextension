/*global define */
define([
    '../Button/Button.js',
    'framework/ModelStorage.js',
], function (
    Button,
    ModelStorage
) {
    "use strict";

    class StateButton extends Button {

        click() {
            this.goToState(this.scope.state);
        }

        goToState(state) {
            ModelStorage.get('AppStateModel').currentState = state;
        }
    }

    return StateButton;
});
