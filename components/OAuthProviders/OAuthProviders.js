/*global define */
define([
    'framework/AbstractComponent.js',
    'text!./OAuthProviders.html'
], function (
    AbstractComponent,
    template
) {
    "use strict";

    class OAuthProviders extends AbstractComponent {

        get template() {
            return template;
        }
    }

    return OAuthProviders;
});