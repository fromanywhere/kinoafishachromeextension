/*global define */
define([
    'framework/AbstractComponent.js',
    'framework/ModelStorage.js',
    'framework/services/LocalizationService.js',
    'text!./FeedList.html'
], function (
    AbstractComponent,
    ModelStorage,
    LocalizationService,
    template
) {
    "use strict";

    class FeedList extends AbstractComponent {

        get template() {
            return template;
        }
        
        init() {
            this.scrollingNow = false;

            this.bindField('isLoginned', 'UserModel', 'isLoginned');
            this.bindField('scrollPosition', 'FeedModel', 'scrollPosition');
            this.bindField(
                'feed',
                'FeedModel',
                'feed',
                function handleListChange() {
                    if (!this.scope.scrollPosition) {
                        this.filterList();
                        this.render();
                    }
                }.bind(this)
            );

            this.bindField(
                'currentFilterState',
                'FeedModel',
                'currentFilterState',
                function render() {
                    this.filterList();
                    this.render();
                }.bind(this)
            );
        }

        filterList() {
            var now = new Date();
            var today = new Date(now.getFullYear(), now.getMonth(), now.getDate());

            var feedCopy = [];
            var savedFilm;
            var filmDate;
            var lastDate;

            this.list = [];
            this.scope.list = [];

            function sortAndDeduplicate(feed, sortProp) {
                var keys = [];
                var result = [];

                var sorted =  feed.slice()
                    .map((item) => {
                        item.parsedDate = item.date
                            ? new Date(item.date)
                            : today;
                        return item;
                    })
                    .filter((item) => {
                        return item.parsedDate >= today;
                    })
                    .sort((a, b) => {
                        var aCat = a.parsedDate.getTime() - a[sortProp];
                        var bCat = b.parsedDate.getTime() - b[sortProp];

                        return aCat > bCat
                            ? 1
                            : aCat < bCat
                                ? -1
                                : 0;

                    })
                    .forEach((item) => {
                        if (keys.indexOf(item.id) === -1) {
                            keys.push(item.id);

                            filmDate = LocalizationService.getDateString(item.parsedDate);
                            item.formatDate = lastDate !== filmDate
                                ? filmDate
                                : '' ;
                            lastDate = filmDate;

                            item.name = LocalizationService.hyphenate(item.name);

                            result.push(item);
                        }
                    });

                return result;
            }

            switch(this.scope.currentFilterState) {
                case 'Actual':
                    this.list = sortAndDeduplicate(this.scope.feed.released, 'seances');
                    break;
                case 'Premiers':
                    this.list = sortAndDeduplicate(this.scope.feed.premieres, 'want');
                    break;
                case 'Favorites':
                    feedCopy = [].concat(this.scope.feed.released, this.scope.feed.premieres);
                    this.list = sortAndDeduplicate(feedCopy, 'name').filter((item) => {
                        return item.favorite;
                    });
                    break;
            }

            this.scope.list = this.list;
            ModelStorage.get('FeedModel').list = this.list;
        }

        activate() {
            this.holder = this.node.querySelector('.feed-list_hld');
            this.leftArrow = this.node.querySelector('.feed-list_arrow.__left');
            this.rightArrow = this.node.querySelector('.feed-list_arrow.__right');
            this.itemList = this.node.querySelectorAll('.feed-list_i');

            this.scrollPosition = this.scope.scrollPosition;
            this.holder.scrollLeft = this.scrollPosition;
            ModelStorage.get('FeedModel').scrollPosition = 0;

            this.onScroll = function onScroll(e) {
                var horizontalThreshold = 3;

                if ((!this.scrollingNow) && (Math.abs(e.deltaX) < horizontalThreshold)) {
                    e.preventDefault();
                    this.scrollingNow = true;
                    this.holder.scrollLeft += (e.deltaY);
                    this.scrollPosition = this.holder.scrollLeft;
                    this.showArrows();
                }
            }.bind(this);

            this.onClickLeft = this.scrollLeft.bind(this);

            this.onClickRight = this.scrollRight.bind(this);

            this.onClickItem = this.saveScrollPosition.bind(this);

            this.onImgError = function onImgError(e) {
                if (e.target.tagName === 'IMG') {
                    e.target.parentNode.classList.add('__show-title');
                    e.target.parentNode.removeChild(e.target);
                }
            }.bind(this);

            this.onFeedCalendarSelect = function onFeedCalendarSelect(date) {
                for (var i = 0; i < this.scope.list.length; i++) {
                    if (this.list[i].parsedDate.toString() === date.detail) {
                        this.scrollTo(this.itemList[i].offsetLeft);
                        break;
                    }
                }
            }.bind(this);

            this.node.addEventListener('wheel', this.onScroll, false);
            this.node.addEventListener('error', this.onImgError, true);
            this.holder.addEventListener('click', this.onClickItem, true);
            this.leftArrow.addEventListener('click', this.onClickLeft, false);
            this.rightArrow.addEventListener('click', this.onClickRight, false);
            document.addEventListener('feedCalendarSelect', this.onFeedCalendarSelect, false);

            this.showArrows();
        }

        deactivate() {
            this.node.removeEventListener('wheel', this.onScroll);
            this.node.removeEventListener('error', this.onImgError);
            this.holder.removeEventListener('click', this.onClickItem);
            this.leftArrow.removeEventListener('click', this.onClickLeft);
            this.rightArrow.removeEventListener('click', this.onClickRight);
            document.removeEventListener('feedCalendarSelect', this.onFeedCalendarSelect);
        }

        showArrows() {
            if (this.scrollPosition === 0) {
                this.render();
            }

            this.leftArrow.classList.toggle('__active', this.scrollPosition > 0);
            this.rightArrow.classList.toggle('__active', this.scrollPosition < (this.holder.scrollWidth - this.holder.clientWidth));
            this.scrollingNow = false;
        }

        saveScrollPosition() {
            ModelStorage.get('FeedModel').scrollPosition = this.scrollPosition;
        }

        scrollLeft() {
            var step = 100 + 12; // ширина + отступ

            var currentScroll = this.scrollPosition;
            var targetScroll = Math.floor(((currentScroll - 1) / step)) * step;

            this.scrollTo(targetScroll);
        }

        scrollRight() {
            var step = 100 + 12; // ширина + отступ

            var currentScroll = this.scrollPosition;
            var targetScroll = Math.ceil(((currentScroll + 1) / step)) * step;

            this.scrollTo(targetScroll);
        }

        scrollTo(targetScroll) {
            if (!this.scrollingNow) {
                this.scrollingNow = true;

                var currentScroll = this.scrollPosition;
                var diff = targetScroll - currentScroll;

                var scroll = function (progress) {
                    this.scrollPosition = currentScroll + progress * diff;
                    this.holder.scrollLeft = this.scrollPosition;
                }.bind(this);

                this.animate({
                    duration: 500,
                    timing: function(timeFraction) {
                        return Math.sin(timeFraction * Math.PI/2);
                    },
                    draw: scroll,
                    callback: () => {
                        this.showArrows();
                    }
                });
            }
        }

        animate(options) {
            var start = performance.now();
            var callback = options.callback || function () {};

            requestAnimationFrame(function animate(time) {
                // timeFraction от 0 до 1
                var timeFraction = (time - start) / options.duration;
                if (timeFraction > 1) {
                    timeFraction = 1;
                }
                if (timeFraction < 0) {
                    timeFraction = 0;
                }

                // текущее состояние анимации
                var progress = options.timing(timeFraction);

                options.draw(progress);

                if (timeFraction < 1) {
                    requestAnimationFrame(animate);
                } else {
                    callback();
                }

            });
        }

    }

    return FeedList;
});
