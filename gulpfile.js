var gulp = require('gulp');
var del = require('del');
var less = require('gulp-less');
var concat = require('gulp-concat');
var webpack = require('webpack');

gulp.task('clean', function () {
	return del([
		'build/**/*',
		'dist/**/*'
	]);
});

gulp.task('dist', ['css', 'move-res'], function () {

	gulp.src([
		'build/**/*',
		'res/icon.png',
		'res/firefox/manifest.json'
	]).pipe(gulp.dest('dist/firefox'));

	gulp.src([
		'build/**/*',
		'res/icon.png',
		'res/chrome/manifest.json'
	]).pipe(gulp.dest('dist/chrome'));
});

gulp.task('move-plugin', function () {
	return gulp.src([
		'res/icon.png',
		'res/firefox/manifest.json'
		])
		.pipe(gulp.dest('build'));
});

gulp.task('move-res', function () {
	return gulp.src([
			'res/img/**/*.*'
		])
		.pipe(gulp.dest('build/img'));
});

gulp.task('css', function () {
	return gulp.src([
			'res/css/**/*.*',
			'components/**/*.less',
			'components/**/*.css'
		])
		.pipe(less())
		.pipe(concat('style.css'))
		.pipe(gulp.dest('build'));
});

gulp.task("watch-css", ['css'], function () {
	gulp.watch([
		'res/css/**/*.*',
		'components/**/*.less',
		'components/**/*.css'
	], ['css']);
});

gulp.task("webpack", function(callback) {
	// run webpack
	return webpack(
		require('./webpack.config.js'),
		function(err) {
			if (err) {
				console.log(err);
			}
			gulp.start('dist');
			callback();
		});
});

gulp.task("watch-webpack", ['watch-css'], function() {
	// run webpack
	return webpack(Object.assign({
	        	watch: true
			},
			require('./webpack.config.js')
		),
		function(err) {
			if (err) {
				console.log(err);
			}
		});
});

gulp.task('default', ['clean'], function () {
	gulp.start('webpack');
});

gulp.task('watch', function () {
	gulp.start('css', 'move-res', 'move-plugin', 'watch-webpack');
});