Browser plugin to display kinoafisha.info feed updates.

# Build

Run *npm i* on directory root. Probably you may need to install gulp globally running *sudo npm i gulp -g*.
Then you can build browser-ready bundle with *gulp*. Build result will be placed to /dist/ and will contain chrome and firefox versions. All the difference between them is only manifest.json file.
For development process you have to use *gulp watch*, which build non-production version to /build/.

# Publish
Dear Mozilla AMO team! To build .xpi file you may to zip *content* of /dist/firefox/, not the folder. The reason is your automated validation process.