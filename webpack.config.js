var path = require('path');
var webpack = require("webpack");
var CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
    entry: './main.js',
    output: {
        filename: 'bundle.js',
        path: './build'
    },
    resolve: {
        alias: {
            doT: "framework/external/doT.js"
        },
        root: [
            path.resolve('./')
        ]
    },
    plugins: [
        new webpack.optimize.LimitChunkCountPlugin({
            maxChunks: 1
        }),
        new CopyWebpackPlugin([
            {
                from: 'index.html',
                to: 'index.html'
            },
            {
                from: 'oauth.js',
                to: 'oauth.js'
            }
        ])
    ]
};